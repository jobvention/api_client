var restify = require('restify');

var fs = require('fs');
var util = require('util');
// Creates a JSON client
var client = restify.createJsonClient({
    url: 'https://api.talentsonar.com'
});

const commandLineArgs = require('command-line-args')
 
const optionDefinitions = [
  { name: 'filepath', alias: 'f', type: String},
  { name: 'filename', type: String},
  { name: 'name', alias: 'n', type: String },
  { name: 'async', type: Boolean },
  { name: 'output', alias: 'o', type: String }
]


const options = commandLineArgs(optionDefinitions)
if(!options.filepath || !options.filename){
    console.log("Invalid Resume File Parameters");
    process.exit();
}


var data = new Buffer(fs.readFileSync(options.filepath)).toString('base64');
var jsonData = {
        talent:{
            name: options.name ? options.name : ""
        }, // optional
        options:{
            hideEmployers: true, // employer & titles
            hideSchools : true,  // school names
        }, // extra options
        file:{
            content: data,
            name: options.filename
        }
    };


client.headers.authorization = 'Bearer YOUR_BEARER_TOKEN_HERE';

if(options.async){
    jsonData.webhook = "https://api.talentsonar.com/v1/webhook";
    client.post('/v1/files/resumes/async', jsonData, function(err, req, res, obj) {
        if(err){
            console.log(err)
        }
    });
}else{
    client.post('/v1/files/resumes', jsonData, function(err, req, res, obj) {
        if(!err){
            if(obj.redacted){
                var redactedContent = Buffer.from(obj.redacted, "base64");
                fs.writeFileSync(options.output ? options.output : "redacted_resum.html", redactedContent);
            }
        }else{
            console.log(err)
        }
    });    
}

